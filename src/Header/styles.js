import { makeStyles } from '@material-ui/core';

export const makeClasses = makeStyles({
  root: {
    width: '100%',
    padding: '10px 2vw',
    backgroundColor: '#43a047',
  },

  text: {
    fontWeight: 400,
    color: 'white',
  },
});
