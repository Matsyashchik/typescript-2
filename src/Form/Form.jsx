import React, {
  useState,
  useImperativeHandle,
  forwardRef,
  useRef,
} from 'react';

import { Input, Button } from '@material-ui/core';

export const Form = forwardRef(({ onAddTodo }, ref) => {
  const [value, setValue] = useState('');

  const inputRef = useRef(null);

  useImperativeHandle(ref, () => ({
    focus: () => {
      inputRef.current && inputRef.current.focus();
    },
  }));

  const onChange = event => {
    setValue(event.target.value);
  };

  const onSave = () => {
    const newTodo = {
      id: Date.now().toString(),
      text: value,
      completed: false,
    };
    onAddTodo(newTodo);
    setValue('');
  };

  const onKeyPress = event => {
    if (event.key === 'Enter') {
      onSave();
    }
  };

  return (
    <div>
      <Input
        inputRef={inputRef}
        value={value}
        onChange={onChange}
        onKeyPress={onKeyPress}
      />
      <Button onClick={onSave}>Сохранить</Button>
    </div>
  );
});
