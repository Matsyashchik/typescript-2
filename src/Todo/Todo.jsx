import React from 'react';

import { FormControlLabel, Checkbox, Button } from '@material-ui/core';

import { makeClasses } from './styles';

export const Todo = ({ id, completed, onCheck, onRemove, children }) => {
  return (
    <div>
      <FormControlLabel
        classes={{ label: makeClasses({ completed }).label }}
        control={
          <Checkbox
            value={completed}
            onChange={() => {
              onCheck();
            }}
          />
        }
        label={children}
        name={id}
      />
      <Button
        onClick={() => {
          onRemove();
        }}
      >
        Удалить
      </Button>
    </div>
  );
};
