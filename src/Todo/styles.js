import { makeStyles } from '@material-ui/core';

export const makeClasses = makeStyles({
  label: props => {
    return {
      textDecoration: props.completed ? 'line-through' : 'none',
    };
  },
});
